FROM registry.ethz.ch/k8s-let/notebooks/jh-notebook-base-ethz:3.0.0-16

USER root

RUN mamba install -c defaults -c conda-forge \
  boost-histogram \
  root \
  && \
  mamba clean --all

USER 1000
